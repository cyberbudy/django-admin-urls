# django-admin-urls

Set of shortcuts to get admin urls for objects/models

### Installation

`$ pip install django-admin-urls`

### Available shortcuts

1. `admin_add_url` - Returns admin add url for a given model.
2. `admin_change_url` - Returns admin change url for a given model instance.
3. `admin_delete_url` - Returns admin delete url for a given model instance.
4. `admin_index_url` - Returns admin dashboard url.
