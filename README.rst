
django-admin-urls
=================

Set of shortcuts to get admin urls for objects/models

Available shortcuts
^^^^^^^^^^^^^^^^^^^


#. ``admin_add_url`` - Returns admin add url for a given model.
#. ``admin_change_url`` - Returns admin change url for a given model instance.
#. ``admin_delete_url`` - Returns admin delete url for a given model instance.
#. ``admin_index_url`` - Returns admin dashboard url.
